﻿USE practica1;

                           -- PRACTICA TRES --
-- 1 visualizar el número de empleados de cada departamento. Utilizar group by para agrupar por departamento.
SELECT COUNT(*) numemple, e.emp_no numdepart FROM emple e GROUP BY e.dept_no;

-- 2 visualizarr los departamentos con más de 5 empleados. Utilizar group by para agrupar por departamento y 
-- havin para establecer la condición sobre los grupos. 
SELECT e.dept_no numdepart, e.emp_no empleados FROM emple e GROUP BY e.dept_no HAVING e.emp_no>5;

-- 3 hallar la media de los salarios de cada departamento(utilizar la función avg y group by)
  SELECT e.dept_no departamento, e.salario media_salarios FROM emple e GROUP BY e.dept_no HAVING AVG( e.salario);
  
-- 4 visualizar el nombre de los empleados vendedores del departamento ventas (nombre del departamento=ventas, oficio=vendedor.
  SELECT e.apellido FROM depart d JOIN emple e ON d.dept_no = e.dept_no WHERE d.dnombre ='VENTAS' AND e.oficio='VENDEDOR' ;
  
-- 5 visualizar el número de vendedores del departamento ventas (utilizar la función count sobre la consulta anterior)
  SELECT COUNT(*) FROM emple e JOIN depart d ON e.dept_no = d.dept_no WHERE d.dnombre='VENTAS' AND e.oficio='VENDEDOR';
  
-- 6 visualizar los oficios de los empleados del departamento ventas
  SELECT e.oficio FROM emple e INNER JOIN depart d ON e.dept_no = d.dept_no WHERE d.dnombre='VENTAS';
  
-- 7 a partir de la tabla "emple", visualizar el número de empleados de cada departamento cuyo oficio sea "empleado" 
 -- utilizar group by para agrupar por departamento. En la clausula where habrá que indicar que el oficio es "empleado"                 
SELECT COUNT(*), e.dept_no FROM emple e WHERE e.oficio='empleado' GROUP BY e.dept_no;

-- 8 visualizar el departamento con más empleados
  SELECT d.dept_no, COUNT(*) numempleados, d.dnombre FROM depart d JOIN emple e ON d.dept_no = e.dept_no GROUP BY d.dept_no;
  SELECT MAX( c1.dnombre) maximo FROM (SELECT d.dept_no, COUNT(*) numempleados, d.dnombre FROM depart d JOIN emple e 
  ON d.dept_no = e.dept_no GROUP BY d.dept_no) c1;
  
-- 9 mostrar los departamentos cuya suma de salarios sea mayor que la media de salarios de todos los empleados
  SELECT e.dept_no, SUM( e.salario) sumadesalario, AVG( e.salario) mediadesalario FROM emple e GROUP BY e.dept_no 
  HAVING SUM( e.salario)> AVG( e.salario) ;     
               
-- 10 para cada oficio obtener la suma de salarios
  SELECT SUM( e.salario) sumadesalariospor, e.oficio FROM emple e GROUP BY e.oficio;

-- 11 visualizar la suma d salarios de cada oficio del departamento de ventas
 SELECT e.oficio, SUM(e.salario) FROM emple e WHERE e.dept_no=(SELECT d.dept_no FROM depart d WHERE d.dnombre="ventas")  GROUP BY e.oficio;
-- otra forma
 SELECT e.oficio, SUM(e.salario) FROM emple e JOIN depart d ON e.dept_no = d.dept_no 
WHERE d.dnombre="ventas"  GROUP BY e.oficio;
  
-- 12 visualizar el número de departamento que tenga mas empleados cuyo oficio sea empleado
  -- subconsulta
  SELECT COUNT(*), e.dept_no, e.oficio FROM emple e WHERE e.oficio='EMPLEADO' GROUP BY e.dept_no;
  -- consulta final
  SELECT COUNT(*) numempleados, MAX( c1.dept_no) numdepartamento, c1.oficio
  FROM (SELECT COUNT(*), e.dept_no, e.oficio FROM emple e WHERE e.oficio='EMPLEADO' GROUP BY e.dept_no) c1;
  
 -- 13 mostrar el número de oficios distintos de cada departamento
  SELECT COUNT(DISTINCT e.oficio) oficio, e.dept_no departamento FROM emple e GROUP BY e.dept_no;
  
-- 14 mostrar los departamentos que tengan más de dos personas trabajando en la misma profesión
  SELECT e.oficio, e.dept_no, e.emp_no FROM emple e WHERE e.emp_no>2  GROUP BY e.oficio;
  
-- 28 a partir de la tabla emple y depart mostrar los datos del departamento que no tiene ningún empleado
  SELECT d.dept_no, d.dnombre, d.loc FROM depart d LEFT JOIN emple e ON d.dept_no = e.dept_no WHERE e.dept_no IS NULL;
  
-- 29 mostrar el número de empleados de cada departamento.
--  En la salida se debe mostrar también los departamentos que no tienen ningún empleado
 SELECT d.dept_no numerodedepartamento, e.emp_no numerodempleados FROM depart d LEFT JOIN emple e 
 ON d.dept_no = e.dept_no GROUP BY d.dept_no;

-- 30 obtener la suma de salarios de cada departamento, mostrando las columnas ndepart, suma salarios y dnombre.
  -- En el resultado también se deben mostrar los departamentos que no tienen empleados
  SELECT d.dept_no numerodepartamento, d.dnombre, SUM( e.salario) sumadesalarios  FROM depart d LEFT JOIN emple e 
  ON d.dept_no = e.dept_no GROUP BY d.dept_no ;              